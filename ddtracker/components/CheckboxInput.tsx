import { type } from "os";
import { ChangeEvent, ChangeEventHandler, FC, useId } from "react";

export type CheckboxChangeHandler = (event: ChangeEvent, checked: boolean) => void;

export interface CheckboxInputProps {
    name?: string,
    onChange?: CheckboxChangeHandler,
    checked: boolean,
    label?: string
}

const CheckboxInput: FC<CheckboxInputProps> = ({
    label, name, checked, onChange
}) => {
    const id = useId()
    if (name == null) name = id;
    const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
        if (onChange) onChange(event, event.target.checked)
    }
    return (
        <div className="flex flex-row justify-center gap-2 mb-3">
            <input
                type={'checkbox'}
                className=" align-middle border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200  bg-no-repeat bg-center bg-contain float-left cursor-pointer"
                id={name}
                name={name}
                onChange={handleChange}
                checked={checked} />
                <label  htmlFor={name} className="justify-center align-middle block uppercase tracking-wide text-gray-700 text-xs">
                    {label}
                </label>
        </div>
    )
}

export default CheckboxInput