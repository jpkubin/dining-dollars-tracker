import { ReactNode, FC, MouseEventHandler, useMemo } from "react"

interface ButtonProps {
    children: ReactNode,
    color?: 'red' | 'green',
    outlined?: boolean,
    dashed?: boolean,
    type?: 'button' | 'submit' | 'reset',
    onClick?: MouseEventHandler<HTMLButtonElement>,
    fullWidth?: boolean
}

const Button: FC<ButtonProps> = ({
    children, outlined, color, type, onClick, dashed, fullWidth
}) => {
    const colorClasses = useMemo(() => {
        switch(color) {
            case 'red':
                return "bg-red-300 hover:bg-red-500"
            case 'green':
                return "bg-green-300 hover:bg-green-500"
            default:
                return ""
        }
    }, [color])
    return (
        <button
            type={type}
            onClick={onClick}
            className={
                `${fullWidth ? "w-full " : ' '} focus:outline-none block align-baseline mb-4 text-center py-3 px-2 rounded ${
                    outlined ? `border border-slate-300 text-slate-300 hover:border-white hover:bg-slate-200 hover:text-white ` : `text-white ${colorClasses}`
                }${
                    dashed ? `border-dashed` : ''
                }`
            }
        >
            {children}
        </button>
    )
}

Button.defaultProps = {
    outlined: false,
    dashed: false,
    color: 'red'
}

export default Button