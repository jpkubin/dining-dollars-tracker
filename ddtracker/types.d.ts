type Semester = {
    start: Date,
    end: Date,
    breaks: [Date, Date][],
    snapshots: [Date, number][]
}