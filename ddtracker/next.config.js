/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  basePath: process.env.NODE_ENV === 'production' ? '/dining-dollars-tracker' : ''
}

module.exports = nextConfig
