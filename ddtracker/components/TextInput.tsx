import { ChangeEvent, ChangeEventHandler, FC, useId } from "react";

export type TextInputType = 'text' | 'number' | 'date' | 'datetime-local' | 'email' | 'month' | 'password' | 'search' | 'time' | 'url' | 'week'

export type TextInputChangeHandler = (event: ChangeEvent, value: string) => void

export interface TextInputProps {
    type?: TextInputType,
    name?: string,
    placeholder?: string | number,
    onChange?: TextInputChangeHandler,
    value?: string | number,
    label?: string,
    disabled?: boolean
}

const TextInput: FC<TextInputProps> = ({
    label, type, name, onChange, value, placeholder, disabled
}) => {
    const id = useId()
    if (name == null) name = id;
    const handleChange: ChangeEventHandler<HTMLInputElement> = (event) => {
        if (onChange) onChange(event, event.target.value)
    }
    return (
        <>
            {label && <label htmlFor={name} className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                {label}
            </label>}
            <input
                type={type ?? 'text'}
                className={`block border border-grey-light w-full p-3 rounded mb-4 ${disabled && 'bg-slate-100 text-gray-400'} `}
                id={name}
                name={name}
                onChange={handleChange}
                value={value || ''}
                disabled={disabled}
                placeholder={placeholder?.toString()} />
        </>
    )
}

export default TextInput