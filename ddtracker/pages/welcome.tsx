import Head from 'next/head'
import Image from 'next/image'
import * as d from 'date-fns'
import { useEffect, useMemo, useState } from 'react'
import TextInput from '../components/TextInput'
import DateInput from '../components/DateInput'
import CheckboxInput from '../components/CheckboxInput'
import Button from '../components/Button'
import { useRouter } from 'next/router'

export function nthWeekday(n: number, weekday: number, month: number, year: number) {
  const firstOfMonth = new Date(year, month, 1)
  if (n < 0) {
    n = d.getWeeksInMonth(firstOfMonth) + n - 1
  }
  const dayDif = weekday - firstOfMonth.getDay();
  const firstDayOfMonth = d.addDays(firstOfMonth, (dayDif < 0) ? dayDif + 7 : dayDif)
  return d.addWeeks(firstDayOfMonth, n)
}

const getSemester = (date?: Date): Omit<Semester, 'snapshots'> => {
  if (!date) date = new Date();
  if (d.getMonth(date) >= 6) {
    var start = nthWeekday(-1, 0, 7, d.getYear(date))
    var end = nthWeekday(-2, 2, 11, d.getYear(date))
    const fallBreakStart = nthWeekday(2, 4, 9, d.getYear(date));
    const thanksgivingBreakStart = nthWeekday(-2, 6, 10, d.getYear(date));
    var breaks: [Date, Date][] = [
      [fallBreakStart, d.addDays(fallBreakStart, 4)],
      [thanksgivingBreakStart, d.addDays(thanksgivingBreakStart, 8)]
    ]
  } else {
    var start = nthWeekday(-2, 0, 0, d.getYear(date))
    var end = nthWeekday(2, 2, 4, d.getYear(date))
    const springBreakStart = nthWeekday(1, 6, 2, d.getYear(date))
    var breaks: [Date, Date][] = [
      [springBreakStart, d.addDays(springBreakStart, 8)]
    ]
  }
  return { start, end, breaks }
}

export default function Home() {
  const router = useRouter();
  const sem = useMemo(() => getSemester(), []);
  const [total, setTotal] = useState<number>(400)
  const [current, setCurrent] = useState<number>(400)
  const [semStart, setSemStart] = useState(sem.start);
  const [semEnd, setSemEnd] = useState(sem.end);
  const [breaks, setBreaks] = useState(sem.breaks);
  const [currentIsUnknown, setCurrentUnknown] = useState(false);
  const save = () => {
    const start: [Date, number] = [semStart, total];
    const snapshot: [Date, number] = [new Date(), current];
    const semData: Semester = {
      start: semStart,
      end: semEnd,
      breaks,
      snapshots: (currentIsUnknown ? [start] : [start, snapshot])
    }
    let localDataJSON = localStorage.getItem('dd-semesters')
    if (localDataJSON) {
      var localData = JSON.parse(localDataJSON) as Semester[]
      localData.push(semData)
    } else {
      var localData = [semData]
    }
    localStorage.setItem('dd-semesters', JSON.stringify(localData))
    router.push('/')
  }
  const setBreak = (n: number, date: Date, setEnd: boolean) => setBreaks(b => {
    b = [...b]
    b[n][+setEnd] = date
    return b
  })
  const deleteBreak = (n: number) => setBreaks(b => {
    b = [...b]
    b.splice(n)
    return b
  })
  const newBreak = () => setBreaks(b => {
    b = [...b]
    let last = b[b.length - 1]
    b.push([d.addDays(last[1], 1), d.addDays(last[1], 2)])
    return b
  })
  useEffect(() => console.log(semStart), [semStart])
  return (
    <div className="bg-slate-100 min-h-screen flex flex-col">
      <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <div className="my-4 bg-white px-6 py-8 rounded shadow-md text-gray-700 w-full">
          <h1 className="mb-8 text-3xl text-center font-bold">Dining Dollars Tracker</h1>
          <DateInput label={'Plan Start Date'} value={semStart} onChange={(e, v) => setSemStart(v ?? sem.start)} />
          <DateInput label={'Plan End Date'} value={semEnd} onChange={(e, v) => setSemEnd(v ?? sem.end)} />
          <TextInput label={'Total Dining Dollars'} type='number' value={total} onChange={(e, v) => setTotal(parseInt(v) || 0)} />
          <TextInput label={'Current Dining Dollars'} type='number' disabled={currentIsUnknown} value={current} onChange={(e, v) => setCurrent(parseInt(v) || 0)} />
          <CheckboxInput label="I don't know how many I have" checked={currentIsUnknown} onChange={(e, c) => setCurrentUnknown(c)} />
          <div className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
            Breaks
          </div>
          {breaks.map(([start, end], i) =>
            <div className="flex gap-1 align-middle justify-between" key={start.toISOString()+end.toISOString()}>
              <DateInput value={start} onChange={(e, d) => d && +d < +breaks[i][1] && setBreak(i, d, false)} />
              <DateInput value={end}  onChange={(e, d) => d && +d > +breaks[i][0] && setBreak(i, d, true)}/>
              <Button onClick={() => deleteBreak(i)} color='red'>X</Button>
            </div>
          )}
          <Button outlined dashed fullWidth onClick={newBreak}>+ New Break</Button>
          <Button fullWidth onClick={save} color='green'>Continue</Button>
          </div>
      </div>
    </div>
  )
}
