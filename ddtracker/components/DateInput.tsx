import { ChangeEvent, ChangeEventHandler, FC } from "react"
import TextInput, { TextInputChangeHandler } from "./TextInput";
import * as d from 'date-fns'


type DateInputChangeHandler = (event: ChangeEvent, date: Date | undefined) => void;

interface DateInputProps {
    name?: string,
    onChange?: DateInputChangeHandler,
    value: Date,
    label?: string
}

const DateInput: FC<DateInputProps> = ({name, onChange, value, label}) => {
    const handleChange: TextInputChangeHandler = (e, v) => {
        if (onChange) onChange(e,  v ? new Date(v): undefined)
    }
    return (
        <TextInput label={label} type='date' value={d.format(value, 'yyyy-MM-dd')} onChange={handleChange} />
    )
}

export default DateInput