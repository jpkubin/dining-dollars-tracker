import { NextPage } from "next"
import { useRouter } from "next/router"
import { useEffect, useMemo, useState } from "react"
import { Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts"
import * as d from 'date-fns'

const getSemesters = () => {
    if (typeof window !== 'undefined') {
        const json = localStorage.getItem('dd-semesters');
        if (json == null) return null;
        const semesters: Semester[] = JSON.parse(json).map((sem: Semester) => ({
            snapshots: sem.snapshots.map<[Date, number]>(([d, n]) => [new Date(d), n]),
            breaks: sem.breaks.map<[Date, Date]>(([s, e]) => [new Date(s), new Date(e)]),
            start: new Date(sem.start),
            end: new Date(sem.end)
        }))
        return semesters;
    }
}

const Page = () => {
    const router = useRouter();
    const [semesters, setSemesters] = useState(getSemesters);
    const [semIndex, setSemIndex] = useState(-1);
    useEffect(() => {
        if (semesters == undefined) return;
        const now = new Date()
        const sem = semesters?.findIndex(sem => sem.start <= now && sem.end > now);
        if (sem == undefined || sem == -1) {
            router.push('/welcome')
        }
        setSemIndex(sem ?? -1);
    }, [semesters, router])
    const semester = useMemo(() => semesters && semesters[semIndex], [semIndex, semesters]);
    const snapshots = useMemo(() => {
        if (!semester) return;
        const { snapshots, breaks } = semester;
        const output = [...snapshots];
        for (let [breakStart, breakEnd] of breaks) {
            let beforeBreak = snapshots.reduce<[Date, number] | null>((prev, cur) => {
                if (cur[0] > breakStart) return prev;
                if (prev == null) return cur;
                if (+breakStart - +cur[0] < +breakStart - +prev[0]) {
                    return cur;
                }
                return prev;
            }, null);
            let afterBreak = snapshots.reduce<[Date, number] | null>((prev, cur) => {
                if (cur[0] < breakEnd) return prev;
                if (prev == null) return cur;
                if (+cur[0] - +breakEnd < +prev[0] - +breakEnd) {
                    return cur;
                }
                return prev;
            }, null);
            if (!(beforeBreak && afterBreak)) continue;
            let breakLength = +breakEnd - +breakStart;
            let dateDif = +afterBreak[0] - +beforeBreak[0] - breakLength;
            let amountDif = beforeBreak[1] - afterBreak[1]
            let amount = Math.round(afterBreak[1] + (((+breakStart - +beforeBreak[0]) / dateDif) * amountDif));
            output.push(
                [breakStart, amount],
                [breakEnd, amount]
            )
        }
        console.log(output)
        return output.sort((a, b) => +a[0] - +b[0]).map(([date, amount]) => ({date: +date, amount}))
    }, [semester]);
    return (
        <div className="bg-slate-100 min-h-screen flex flex-col">
            <div className="container max-w-lg mx-auto flex-1 flex flex-col items-center justify-center px-2">
                <div className="my-4 bg-white px-6 py-8 rounded shadow-md text-gray-700 w-full">
                    <h1 className="mb-8 text-3xl text-center font-bold">Dining Dollars Tracker</h1>
                    {snapshots ? (
                        <div className='flex justify-center'>
                            <LineChart width={300} height={200} data={snapshots}>
                                <Line type="linear" stroke="#8884d8" dataKey='amount'></Line>
                                <XAxis dataKey='date' />
                                <YAxis />
                                <Tooltip />
                            </LineChart>
                        </div>
                    ) : "Loading..."}
                </div>
            </div>
        </div>
    );
}

export default Page